README
------
This is the README file from the PARSEME verbal multiword expressions (VMWEs) corpus for Greek (EL), edition 1.3. See the wiki pages of the [PARSEME corpora](https://gitlab.com/parseme/corpora/-/wikis/) initiative for the full documetation of the annotation principles.

The present Greek data result from an update and an extension of the Greek part of the [PARSEME 1.2 corpus](http://hdl.handle.net/11234/1-3367). For the changes with respect to the 1.2 version, see the change log below.

The raw corpus from edition 1.2 is not released in the present directory, but can be downloaded from a [dedicated page](https://gitlab.com/parseme/corpora/-/wikis/Raw-corpora-for-the-PARSEME-1.2-shared-task).

Source Corpora
---------------------------

All the annotated data come from the following sources:

*	`EL-POLYTROPON corpus`: subpart of the POLYTROPON [corpus](https://www.researchgate.net/publication/367233460_Annotating_Greek_VMWEs_in_running_text_a_piece_of_cake_or_looking_for_a_needle_in_a_haystack) of Modern Greek. 
*	`GDT`: subpart of the [Greek Dependency Treebank](http://gdt.ilsp.gr/). Sentences have the identifier `gdt-`.

Format
-------------

The data are in [.cupt](http://multiword.sourceforge.net/cupt-format) format. The following tagsets have been used:

* column 4 (POS): [UD POS-tags](http://universaldependencies.org/u/pos) version 2.5.
* column 5 (XPOS): GDT-UD tagset. Both tagsets are equivalent.
* column 6 (FEATS): [UD features](http://universaldependencies.org/u/feat/index.html) version 2.5 (as of March 2020).
* column 8 (DEPREL) [UD dependency relations](http://universaldependencies.org/u/dep) version 2.5 (as of March 2020).
column 11 (PARSEME:MWE): [PARSEME VMWE categories](http://parsemefr.lif.univ-mrs.fr/parseme-st-guidelines/1.2/?page=030_Categories_of_VMWEs) version 1.2.

Text genres, origins and annotations
--------------------------------------------------------------
The corpus comprises textual data that originate from various online sources (the Greek wikipedia, extracts from newspapers, magazines and news portals). Sources of textual data are: kathimerini, tovima, tanea, avgi, protothema, in.gr, iefimerida, efsyn, protagon, capital, newsit, espresso. They fall into the following text genres: newswire texts, press releases, opinion articles, and popular science articles. The GDT also contains parliament debates.

The VMWE annotations (in column 11) were performed by multiple annotators. The following [categories](http://parsemefr.lif.univ-mrs.fr/parseme-st-guidelines/1.2/?page=030_Categories_of_VMWEs) are annotated: VID, LVC.full, LVC.cause, VPC.full.

The lemmatization and morpho-syntactic annotation of the sentences originating from the GDT was performed manually. For the rest of the data, annotations were performed automatically using [DPipe 2](https://ufal.mff.cuni.cz/udpipe/2) that relied on the model `greek-gdt-ud-2.5-191206`.  

Tokenization was performed automatically.

* Contractions: Most contractions are kept as a single unit (not-split).  Only the forms _στου_ (_στης_, _στον_, _στη_, _στην_, _στο_, _στων_, _στους_, _στις_, _στα_) are split as two tokens _σ_ and _του_ (_της_, _τον_, _τη_, _την_, _το_, _των_, _τους_, _τις_, _τα_).

Statistics
-------
To know the number of annotated VMWEs of different types and with different properties (length, continuity, etc.), use these scripts: [mwe-stats.py](https://gitlab.com/parseme/utilities/-/blob/master/st-organizers/corpus-statistics/mwe-stats.py) and [mwe-stats-simple.py](https://gitlab.com/parseme/utilities/-/blob/master/st-organizers/corpus-statistics/mwe-stats-simple.py). 

Companion raw corpus
------------------------------------------
The manually annotated corpus, described above, is accompanied by a large "raw" corpus (meant for automatic discovery of VMWEs. In this companion raw corpus, VMWEs are not annotated and morphosyntactic annotation is performed automatically. The raw data were drawn from the Greek section of the [Leipzig Corpus](https://wortschatz.uni-leipzig.de/en/download/) collection (Goldhahn et al., 2012), i.e., the news and wikipedia subcorpora, as well as a sub-part of the CoNLL 2017 shared task [raw corpora](http://hdl.handle.net/11234/1-1989). The corpus can be summarised as follows:

* size (uncompressed)	: 2,15 GB
* sentences				: 1,33 M
* tokens					: 25,6 M
* tokens/sentence		: 24.5
* format					: [CoNLL-U](www.universaldependencies.org/format.html)
* source					: [Leipzig Corpus](https://wortschatz.uni-leipzig.de/en/download/) collection for Greek
* genre					: Wikipedia, newswire texts
* POS-tagging			: automatically produced using [UDPipe](https://ufal.mff.cuni.cz/udpipe/2) trained on the greek-gdt-ud-2.5-220711 model
* compatibility with the manually annotated corpus: same tagset - different UDPipe model used.

Licence
-------
The VMWEs annotations (column 11) are distributed under the terms of the [CC-BY v4 license](https://creativecommons.org/licenses/by/4.0/).
The lemmas, POS-tags, morphological and features (columns 1-6), are distributed under the terms of the [CC-BY-NC-SA v4.0](https://creativecommons.org/licenses/by-sa/4.0/) for the sentences from the GDT-UD. For the rest of the sentences, annotations are distributed under the terms of the [GNU GPL v.3](https://www.gnu.org/licenses/gpl.html).

Authors
-------
Anna Kanellopoulou annotated new data in version 1.3. Voula Giouli made corrections towards enhancing the existing annotations. Voula Giouli, Vassiliki Foufi, Aggeliki Fotopoulou, and Stella Markantonatou annotated data in versions 1.0, 1.1, and 1.2. Stella Papadelli and Sevasti Louizou contributed to the annotations in versions 1.0 and 1.1 respectively.

Change log
-------------------
- **2023-04-15**:
        - Version 1.3 of the corpus was released on LINDAT.
	- Changes with respect to version 1.2 of the corpus are the following:
		- all files annotated automatically at the pos and lemma level have been reannotated using [UDPipe 2](https://ufal.mff.cuni.cz/udpipe/2) and the greek-gdt-ud-2.10-220711 model
		- correcting errors (after consistency checks)
		- enhancing the corpus with new data.
		- updating the README file

- **2020-07-09**:
	- [Version 1.2](http://hdl.handle.net/11234/1-3367) of the corpus was released on LINDAT
	- Changes with respect to the 1.1 version are the following:
		- annotating new files
		- fixing known bugs in previous annotations
		- providing a companion raw corpus, automatically annotated for morpho-syntax
		- updating the README file.

- **2018-04-30**:
	- [Version 1.1](http://hdl.handle.net/11372/LRT-2842) of the corpus was released on LINDAT
	- Changes with respect to the 1.0 version are the following:
		- extending the corpus with a sub-part of the GDT
		- updating the existing VMWE annotations to comply with PARSEME guidelines eidtion 1.1
		- updating the READEME file.

- **2017-01-20**:
	- [Version 1.0](http://hdl.handle.net/11372/LRT-2282) of the corpus was released on LINDAT.

Contact
-----------------
voula@athenarc.gr

Reference
-----------------
When using this corpus, please cite:

Giouli, V., Foufi, V., and Fotopoulou, A. (2019). Annotating Greek VMWEs in running text: a piece of cake or looking for a needle in a haystack? In Proceedings of the 13th International Conference on Greek Linguistics, London, UK. 

Future work
---------------------
The corpus is constantly under extension and validation. In the future, we plan to: 

* further upgrade the automatically aquired syntactic annotations for part of the corpus;
* update the corpus with new textual data from the [Hellenic National Corpus (HNC)](www.hnc.ilsp.gr), namely, its Golden subcorpus (~100K tokens), that is manually corrected at the lemma and POS level;
* include more genres (i.e., literature);
* render the raw corpus compatible the manually annotated one wrt POS-tagging (UDPipe version and model used).


References
-------------------
Goldhahn, D., Eckart, T., & Quasthoff, U. (2012). Building Large Monolingual Dictionaries at the Leipzig Corpora Collection: From 100 to 200 Languages. In: Proceedings of the 8th International Language Ressources and Evaluation (LREC'12).



